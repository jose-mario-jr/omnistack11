const express = require("express")
const cors = require("cors")
const routes = require("./routes")

const app = express()

app.use(cors())
// app.use(cors({
//   origin: 'http://meuapp.com'
// }))

app.use(express.json())
// params:
//   query params => route?item=value, acessa em request.query
//   route params => app.post("/route/:id", acessa com request.params
//   request body => corpo da requesicao, acessa em request.body
app.use(routes)

app.listen(3333)
